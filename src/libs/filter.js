export default {
  /**
   * @description 补齐4位小数
   * @param {number} value 要转换的数
   * @param {number} len 保留长度
   * @param {boolean} force 是否强制保留小数(true不保留,false保留)
   * @author 一枚前端
   */
  money: function(value, len, force) {
    try {
      if (!value) value = 0;
      if (typeof value !== "number") {
        value = Number(value);
        if (isNaN(value)) throw 0;
      }
      let oldLen = 4;
      if (len || len === 0) {
        oldLen = len;
        len = ("1" + Array(len + 1).join("0")) * 1;
      } else {
        len = 10000;
      }
      return !force
        ? (Math.trunc(value * len) / len).toFixed(oldLen)
        : Math.trunc(value * len) / len;
    } catch (error) {
      return 0;
    }
  },
  /**
   * @description 补齐图片路径
   * @param {string} value 图片路径
   * @author 一枚前端
   */
  makeImgSrc: function(value, division) {
    try {
      if (!value) throw "";
      // return `http://192.168.0.196:88/storage/${value}`;
      return `${value}`;
    } catch (error) {
      return "";
    }
  },
  /**
   * @description 补齐图片路径(王龙龙)
   * @param {string} value 图片路径
   * @author 一枚前端
   */
  makeImgSrcWll: function(value, division) {
    try {
      if (!value) throw "";
      return `http://fk.keep-sport.top/${value}`;
      // return `http://192.168.0.215:881/${value}`;
    } catch (error) {
      return "";
    }
  },
  /**
   * @description 根据列过滤
   * @param {Array|Object} data 图片路径
   * @param {any} columnName 列名称
   * @param {any} text 值
   * @author 一枚前端
   */
  filterColumn: function(data, columnName, text) {
    try {
      let temp = [];
      (data || []).map(val => {
        if (val[columnName] === text) {
          temp.push(val || []);
        }
      });
      return temp;
    } catch (error) {
      return [];
    }
  },
  /**
   * @description 匹配订单的状态
   * @param {string} type 类型
   * @author 一枚前端
   */
  typeString: function(type) {
    try {
      console.log(type);
      if (!type) throw "";
      // 挂在市场上的状态
      const typeObj = {
        init: "匹配中",
        cannel: "已取消",
        trading: "待付款",
        payed: "已付款",
        complain: "维权中",
        done: "已完成"
      };
      return typeObj[type] || "未知";
    } catch (error) {
      return "未知";
    }
  },
  // 格式化时间
  formatDate(date, fmt) {
    if (date) date = new Date(date);
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        (date.getFullYear() + "").substr(4 - RegExp.$1.length)
      );
    }
    let o = {
      "M+": date.getMonth() + 1,
      "d+": date.getDate(),
      "h+": date.getHours(),
      "m+": date.getMinutes(),
      "s+": date.getSeconds(),
      "af+": date.getMinutes() <= 12 ? "AM" : "PM" //上下午
    };
    for (let k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        let str = o[k] + "";
        fmt = fmt.replace(
          RegExp.$1,
          RegExp.$1.length === 1 ? str : padLeftZero(str)
        );
      }
    }
    return fmt;
  }
};

function padLeftZero(str) {
  return ("00" + str).substr(str.length);
}
