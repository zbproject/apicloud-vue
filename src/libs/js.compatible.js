// web兼容处理(如果页面需要在web上使用则可以在页面引用当前js)
(() => {
    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", "./static/js/jquery.min.1.9.1.js");
    document.body.appendChild(script);
    if (script.addEventListener) {
        script.addEventListener('load', function () {
            apiready && apiready();//兼容初始化
        }, false);
    } else if (script.attachEvent) {
        script.attachEvent('onreadystatechange', function () {
            var target = window.event.srcElement;
            if (target.readyState == 'loaded') {
                apiready && apiready();//兼容初始化
            }
        });
    };
    window.api = {
        pageType: "web",
        deviceId: "x0x0x0x0x0x00000000",
        systemType: "ios",
        connectionType: "wifi",
        deviceModel: "XIAOMI-P30",
        appVersion: "1.0.0",
        getPrefs: () => {
            return "";
        },
        //兼容其他支持
        toast: (msg) => {
            typeof msg === "object" ? (msg = msg.msg) : "";
            MuseUIMessage.alert(msg,{
                type: "info",
                okLabel: "确认",
                icon: "info"
            });
        },
        openFrame: () => { },
        to: () => { },
        ajax: (param, callback) => {
            $.ajax({
                url: param.url,
                type: "POST",
                dataType: "json",
                data: param.data.values,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                success: (res) => {
                    typeof callback === "function" && callback(res, "");
                }, error: (err) => {
                    typeof callback === "function" && callback("", err);
                }
            })
        }
    }
})();